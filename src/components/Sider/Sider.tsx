import React from "react";
import Sider from "antd/es/layout/Sider";
import Menu from "antd/es/menu";
import Logo from "../../assets/VikramSolar.png";
import styles from "./Sider.module.css";
import { useNavigate } from "react-router-dom";
import {
  DesktopOutlined,
  PieChartOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import { MenuProps } from "antd/es/menu";
import MenuItem from "antd/es/menu/MenuItem";
type SiderProps = {
  collapsed: boolean;
  setCollapsed: React.Dispatch<React.SetStateAction<boolean>>;
};

type MenuItem = Required<MenuProps>["items"][number];

// function getItem(
//   label: React.ReactNode,
//   key: React.Key,
//   icon?: React.ReactNode,
//   children?: MenuItem[]
// ): MenuItem {
//   return {
//     key,
//     icon,
//     children,
//     label,
//   } as MenuItem;
// }

// const items: MenuItem[] = [
//   getItem("Dashboard", "1", <PieChartOutlined />),
//   getItem("Readings", "2", <DesktopOutlined />),
//   getItem("Settings", "sub1", <SettingOutlined />, [
//     getItem("Option 1", "3"),
//     getItem("Option 2", "4"),
//     getItem("Option 3", "5"),
//   ]),
// ];

const VikramSolar = () => {
  const Navigate = useNavigate();

  return (
    <div
      className={styles.card}
      onClick={() => {
        Navigate("/");
      }}
    >
      <img src={Logo} alt="Logo" width={120} />
    </div>
  );
};

const SiderMain: React.FC<SiderProps> = (props: SiderProps) => {
  const Navigate = useNavigate();

  return (
    <Sider
      collapsible
      collapsed={props.collapsed}
      onCollapse={(value) => props.setCollapsed(value)}
      theme="light"
    >
      <div className="demo-logo-vertical" />
      <VikramSolar />
      <Menu defaultSelectedKeys={["1"]} mode="inline">
        <MenuItem
          key="1"
          onClick={() => {
            Navigate("/");
          }}
          icon={<PieChartOutlined />}
        >
          Dashboard
        </MenuItem>
        <MenuItem
          key="2"
          onClick={() => {
            Navigate("/Readings");
          }}
          icon={<DesktopOutlined />}
        >
          Readings
        </MenuItem>
        <MenuItem key="3" icon={<SettingOutlined />}>
          Options
        </MenuItem>
      </Menu>
    </Sider>
  );
};

export default SiderMain;
