import "./App.css";
import MainLayout from "./pages/MainLayout/MainLayout";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import DashboardMain from "./components/DashboardMain/DashboardMain";
import Readings from "./pages/Readings/Readings";
function App() {
  const Router = createBrowserRouter([
    {
      path: "/",
      element: <MainLayout Component={DashboardMain} />,
    },
    {
      path: "/Readings",
      element: <MainLayout Component={Readings} />,
    },
  ]);

  return (
    <>
      <RouterProvider router={Router} />
    </>
  );
}

export default App;
