import React, { useState } from "react";
import SiderMain from "../../components/Sider/Sider.tsx";
import FooterMain from "../../components/FooterMain/FooterMain.tsx";
import { Layout } from "antd";
import HeaderMain from "../../components/Header/Header.tsx";
import styles from "./MainLayout.module.css";

interface Props {
  Component: React.FC;
}

const MainLayout: React.FC<Props> = ({ Component }) => {
  const [collapsed, setCollapsed] = useState(false);

  return (
    <Layout className={styles.siteLayout}>
      <SiderMain collapsed={collapsed} setCollapsed={setCollapsed} />
      <Layout>
        <HeaderMain />
        <Component />
        <FooterMain />
      </Layout>
    </Layout>
  );
};

export default MainLayout;
