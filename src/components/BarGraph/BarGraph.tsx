import React from "react";
import { ApexOptions } from "apexcharts";
import ReactApexChart from "react-apexcharts";

type Prop = {
  heading?: string;
  value?: number;
  change?: string;
};

interface BarGraphState {
  series: {
    name: string;
    data: number[];
    group: string;
  }[];
  options: ApexOptions;
}

class BarGraph extends React.Component<Prop, BarGraphState> {
  constructor(props: Prop) {
    super(props);

    this.state = {
      series: [
        {
          name: "Q1 Budget",
          group: "budget",
          data: [44000, 55000, 41000, 67000],
        },
        {
          name: "Q1 Actual",
          group: "actual",
          data: [48000, 50000, 40000, 65000],
        },
      ],
      options: {
        chart: {
          type: "bar",
          height: 350,
          stacked: true,
        },
        stroke: {
          width: 1,
          colors: ["#fff"],
        },
        dataLabels: {
          formatter: () => "",
        },
        plotOptions: {
          bar: {
            horizontal: false,
          },
        },
        xaxis: {
          categories: ["Q1", "Q2", "Q3", "Q4"],
        },
        fill: {
          opacity: 1,
        },
        colors: ["#80c7fd", "#008FFB", "#80f1cb", "#00E396"],
        yaxis: {
          labels: {
            formatter: (val) => {
              return val / 1000 + "K";
            },
          },
        },
        legend: {
          position: "top",
          horizontalAlign: "left",
        },
      },
    };
  }

  render() {
    return (
      <div>
        <div id="chart">
          <ReactApexChart
            options={this.state.options}
            series={this.state.series}
            type="bar"
            height={350}
          />
        </div>
        <div id="html-dist"></div>
      </div>
    );
  }
}

export default BarGraph;
