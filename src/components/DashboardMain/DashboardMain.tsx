import BreadCrumbMain from "../BreadCrumbMain/BreadCrumbMain";
import SimpleCard from "../Card/Card";
import Graph from "../LineGraph/LineGraph";
import DashboardTable from "../Table/Table";
import BarGraph from "../BarGraph/BarGraph";
import { Layout } from "antd";
import { theme, Flex, Divider } from "antd";
import styles from "./DashboardMain.module.css";

const { Content } = Layout;

const DashboardMain = () => {
  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken();
  return (
    <>
      <Content>
        <div className={styles.breadcrumb}>
          <BreadCrumbMain />
        </div>

        <Flex gap="middle" vertical={false} style={{ margin: 10 }}>
          <SimpleCard heading="Bill" value={1000} change={-10} />
          <SimpleCard heading="Bill" value={1000} change={10} />
          <SimpleCard heading="Bill" value={1000} change={-10} />
          <SimpleCard heading="Bill" value={1000} change={10} />
        </Flex>

        <div className={styles.graph}>
          <Graph />
        </div>
        <div
          style={{
            background: colorBgContainer,
            borderRadius: borderRadiusLG,
          }}
          className={styles.graph}
        >
          <Flex dir="column">
            <DashboardTable />
            <Divider type="vertical" className={styles.divider} />
            <BarGraph />
          </Flex>
        </div>
      </Content>
    </>
  );
};

export default DashboardMain;
