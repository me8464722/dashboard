import { Footer } from "antd/es/layout/layout";
import styles from "./FooterMain.module.css";

const FooterMain = () => {
  return (
    <Footer className={styles.footer}>
      Vikram Solar ©{new Date().getFullYear()}
    </Footer>
  );
};

export default FooterMain;
