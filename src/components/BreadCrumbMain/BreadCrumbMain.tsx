import { Breadcrumb } from "antd";

const BreadCrumbMain = () => {
  return (
    <Breadcrumb>
      <Breadcrumb.Item>User</Breadcrumb.Item>
      <Breadcrumb.Item>Bill</Breadcrumb.Item>
    </Breadcrumb>
  );
};

export default BreadCrumbMain;
