import React from "react";
import { ShoppingCartOutlined, BellOutlined } from "@ant-design/icons";
import { Avatar } from "antd";
import { Header } from "antd/es/layout/layout";
import styles from "./Header.module.css";

const UserContainer = () => {
  return <p className={styles.userContainer}>John Doe</p>;
};

const childrenArray: React.ReactNode[] = [
  <ShoppingCartOutlined className={styles.icon} />,
  <BellOutlined className={styles.icon} />,
  <Avatar
    src="https://www.gravatar.com/avatar/?d=identicon"
    size={32}
    className={styles.avatar}
  />,
  <UserContainer />,
];

const HeaderMain = () => {
  return <Header className={styles.header} children={childrenArray} />;
};

export default HeaderMain;
