import React from "react";
import { Card, Tag, Flex } from "antd";
import { FallOutlined, RiseOutlined } from "@ant-design/icons";
import styles from "./Card.module.css";

type Prop = {
  heading: string;
  value: number;
  change: number;
};

const SimpleCard: React.FC<Prop> = (props: Prop) => (
  <Card className={styles.card}>
    <p>{props.heading}</p>
    <Flex align="baseline" justify="flex-start" gap={20}>
      <p className={styles.flexP}>{props.value}</p>
      <Tag
        style={{
          backgroundColor: props.change > 0 ? "green" : "red",
        }}
        className={styles.tag}
      >
        {props.change > 0 ? <RiseOutlined /> : <FallOutlined />}
        {props.change}%
      </Tag>
    </Flex>
    <p>View Details</p>
  </Card>
);

export default SimpleCard;
